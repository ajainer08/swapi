import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import { Autocomplete } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import './swapi.scss';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Swapi() {

  //States which are used within the component
  const [open, setOpen] = React.useState(false);

  const [data, setdata] = useState(null);

  const [value, setValue] = useState(null);

  const [people, setPeople] = useState([]);


  //Function to hander the close event on Snack bar
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  // Component Did Mount hook
  useEffect(() => {
    getDropdownValues();
  }, []);

  // Component should update hook
  useEffect(() => {
    if (value && value.residents && value.residents.length > 0) {
      getPeopleData(value.residents);
      setOpen(false);
    } else {
      setPeople([]);
      if(value && Object.keys(value).length > 0){
        setOpen(true);
      }
    }
  }, [value]);

  // function to get the data of people base on the planet selection
  const getPeopleData = async (urls) => {
    try {
      let res = await Promise.all(urls.map((e) => fetch(e)));
      let resJson = await Promise.all(res.map((e) => e.json()));
      resJson = resJson.map((e, i) => ({ ...e, id: i }));
      setPeople(resJson);
    } catch (err) {
      console.log(err);
      setPeople([]);
    }
  };

  //function to update the Autocomplete field options
  const getDropdownValues = (string = '') => {
    if(string && string.length > 0){
        fetch("https://swapi.dev/api/planets?search=" + string)
        .then((response) => response.json())
        .then((data) => {
            setdata(data.results);
        });
    }else {
        fetch("https://swapi.dev/api/planets")
        .then((response) => response.json())
        .then((data) => {
            setdata(data.results);
        });
    }
  };

  //Datatable columns definition
  const columnsData = [
    { field: "name", headerName: "Name", width: 150 },
    { field: "gender", headerName: "Gender", width: 150 },
    { field: "height", headerName: "Height", width: 150 },
    { field: "birth_year", headerName: "Birth Year", width: 150 },
    { field: "url", headerName: "Url", width: 300 }
    
  ];

  // Returning JSX HTML
  return (
    <div className="container star-war-container">
      <div className="row">
        <div className="col-md-12">
          <div className="mt-4 header-title">
            <h2 className="text-center">Star War APIs</h2>
          </div>
          <div className="mt-4">
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={data}
              getOptionLabel={(option) => option.name}
              value={value}
              renderInput={(params) => (
                <TextField {...params} label="Select or Type Planet Name" />
              )}
              onInputChange={(event, value, reason) => {
                if (reason === "input" || reason === "clear") {
                    getDropdownValues(value);
                }
              }}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
            />
          </div>
          <div className="mt-2" style={{ height: 600, width: "100%" }}>
            <DataGrid
              rows={people}
              columns={columnsData}
              //checkboxSelection
              initialState={{
                pagination: { paginationModel: { pageSize: 10, page: 0 } },
              }}
              pageSizeOptions={[5, 10, 25]}
            />
          </div>
          <Snackbar
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          >
            <Alert
              onClose={handleClose}
              severity="error"
              sx={{ width: "100%" }}
            >
              People does not exist on the selected planet.
            </Alert>
          </Snackbar>
        </div>
      </div>
    </div>
  );
}
