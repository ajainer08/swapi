import './App.css';
import Swapi from './Swapi/index';

function App() {
  return (
    <div className="App">
        {/* Load Star War Component */}
        <Swapi />
    </div>
  );
}

export default App;
